import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';

@Injectable()
export class JsonPlaceholderService {

  constructor(private http: Http) { }

  public async getUsers(): Promise<object[]> {
    const url = environment.jsonPlaceholderBaseUrl + '/users';

    return new Promise<object[]>((resolve, reject) => {
      this.http.get(url)
        .subscribe((response: any) => {
          if (response.status === 200) {
            resolve(JSON.parse(response._body));
          } else {
            reject(response);
          }
        })
    })
  }

}
