import { ColorValidatorDirective } from './color-validator.directive';
import { FormControl } from '@angular/forms';

const errorObject = {
  validColor: false
};

describe('ColorValidatorDirective', () => {
  it('should create an instance', () => {
    const directive = new ColorValidatorDirective();
    expect(directive).toBeTruthy();
  });

  it('should be a valid HEX Color', () => {
    const directive = new ColorValidatorDirective();
    const formControl = new FormControl();
    formControl.setValue('#fefafe');

    expect(directive.validate(formControl)).toBeNull();
  });

  it('should be an invalid HEX Color', () => {
    const directive = new ColorValidatorDirective();
    const formControl = new FormControl();
    formControl.setValue('#abcdefgh');

    expect(directive.validate(formControl)).toEqual(errorObject);
  });

  it('should be a valid RGB Color', () => {
    const directive = new ColorValidatorDirective();
    const formControl = new FormControl();
    formControl.setValue('rgb(0, 180, 130)');

    expect(directive.validate(formControl)).toBeNull();
  });

  it('should be an invalid RGB Color', () => {
    const directive = new ColorValidatorDirective();
    const formControl = new FormControl();
    formControl.setValue('rgb(a, b, c)');

    expect(directive.validate(formControl)).toEqual(errorObject);
  });
});
