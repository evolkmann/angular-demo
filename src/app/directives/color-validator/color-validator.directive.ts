import { Directive, forwardRef } from '@angular/core';
import { FormControl, NG_VALIDATORS, Validator } from '@angular/forms';

@Directive({
  selector: '[validateColor]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => ColorValidatorDirective), multi: true }
  ]
})
export class ColorValidatorDirective implements Validator {

  constructor() { }

  validate(formControl: FormControl) {
    return this.isValidColorString(formControl.value) ? null : {
      validColor: false
    };
  }

  private isValidColorString(colorString: string): boolean {
    return this.isHexColor(colorString) || this.isRgbColor(colorString);
  }

  private isHexColor(colorString: string): boolean {
    const isHexRegex = /^#([0-9a-f]{3}){1,2}$/i;

    return isHexRegex.test(colorString);
  }

  private isRgbColor(colorString: string): boolean {
    const rgbRegex = /rgb\((\d{1,3}), {0,1}(\d{1,3}), {0,1}(\d{1,3})\)/;

    return rgbRegex.test(colorString);
  }

}
