import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DirectivesDemoComponent } from './components/directives-demo/directives-demo.component';
import { ServicesDemoComponent } from './components/services-demo/services-demo.component';

const routes: Routes = [
  {
    path: 'dashboard', component: DashboardComponent
  },
  {
    path: 'demos', children: [
    {
      path: 'directives', component: DirectivesDemoComponent
    },
    {
      path: 'services', component: ServicesDemoComponent
    }
  ]
  },
  {
    path: '', redirectTo: 'dashboard', pathMatch: 'full'
  },
  {
    path: '**', redirectTo: 'dashboard', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
