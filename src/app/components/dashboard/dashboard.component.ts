import { Component, OnInit } from '@angular/core';
import { Demo } from '../../models/demo.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  demos: Demo[] = [
    {
      name: 'Directives',
      icon: 'spellcheck',
      route: '/demos/directives'
    },
    {
      name: 'Services',
      icon: 'code',
      route: '/demos/services'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
