import { Component, OnInit } from '@angular/core';
import { JsonPlaceholderService } from '../../services/json-placeholder/json-placeholder.service';

@Component({
  selector: 'app-services-demo',
  templateUrl: './services-demo.component.html',
  styleUrls: ['./services-demo.component.css']
})
export class ServicesDemoComponent implements OnInit {

  loading = false;

  users: any;

  constructor(private jsonPlaceholderService: JsonPlaceholderService) { }

  ngOnInit() {
  }

  public getUsers(): void {
    this.loading = true;

    this.jsonPlaceholderService.getUsers()
      .then((users: object[]) => {
        this.users = users;
        this.loading = false;
      })
      .catch((error) => {
        alert(error.message);
        this.loading = false;
      })
  }

}
