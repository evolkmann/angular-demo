import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MdListModule,
  MdIconModule,
  MdToolbarModule,
  MdButtonModule,
  MdInputModule,
  MdProgressBarModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DirectivesDemoComponent } from './components/directives-demo/directives-demo.component';
import { TopNavComponent } from './components/top-nav/top-nav.component';
import { ColorValidatorDirective } from './directives/color-validator/color-validator.directive';
import { ServicesDemoComponent } from './components/services-demo/services-demo.component';
import { JsonPlaceholderService } from './services/json-placeholder/json-placeholder.service';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DirectivesDemoComponent,
    TopNavComponent,
    ColorValidatorDirective,
    ServicesDemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MdListModule,
    MdIconModule,
    MdToolbarModule,
    MdButtonModule,
    MdInputModule,
    FormsModule,
    HttpModule,
    MdProgressBarModule
  ],
  providers: [
    JsonPlaceholderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
