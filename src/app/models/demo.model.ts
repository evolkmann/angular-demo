export interface Demo {
  name: string;
  icon: string;
  route: string;
}
